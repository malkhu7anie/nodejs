const path = require('path');

console.log(path.sep);

const file_path = path.join('/dir', 'dir2', 'test.txt');
console.log(file_path);

const base = path.basename(file_path);
console.log(base);

const absolute = path.resolve(__dirname, 'dir', 'dir2', 'test.txt');
console.log(absolute);
