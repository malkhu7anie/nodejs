const {readFile, writeFile} = require("fs");

console.log("start");
readFile("./dir/first.txt", "utf8", (err, result) => {
    if (err) {
        console.log(err);
        return;
    }
    const first = result;
    readFile("./dir/dir2/text.txt", "utf8", (err, result) => {
        if (err) {
            console.log(err);
            return;
        }
    });
    const second = result;
    writeFile("./dir/res.txt",
        `here is the result: ${first}, ${second}`,
        (err, result) => {
            if (err) {
                console.log(err);
                return;
            }
            console.log("done with this task");
            // console.log(result);
        }
    );
})

console.log("starting next task");
