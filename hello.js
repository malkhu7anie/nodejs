const http = require("http");

const hostname = "127.0.0.1";
const port = 8000;

const server = http.createServer(function (req, res) {
    res.writeHead(200, { "content-type": "text/plain" })
    res.end("hello world\n");
});


server.listen(port, hostname, function() {
    console.log(`serve is running at http://${hostname}:${port}/`)
});
