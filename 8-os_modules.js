const os = require('os');

const user = os.userInfo();

console.log(user);

// returns system uptime in seconds;
console.log(`the system uptime is ${os.uptime()}`);

const current_os = {
    name: os.type(),
    release: os.release(),
    total_mem: os.totalmem(),
    free_mem: os.freemem(),
};

console.log(current_os);
