const {readFileSync, writeFileSync} = require("fs");

console.log("start");
const first = readFileSync("./dir/first.txt", "utf8");
const second = readFileSync("./dir/dir2/text.txt", "utf8");

console.log(first, second);

writeFileSync(
    "./dir/result-sync.txt", 
    `here is the result ${first}, ${second}`,
    { flag: 'a' }
);

console.log("done with this task");
console.log("starting the next one");
